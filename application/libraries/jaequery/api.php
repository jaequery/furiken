<?php
namespace Jaequery;

use Route;
use Input;

class Api {

    public $routes = array();

    public function register() {
        $params = Input::get();
        foreach ($this->routes AS $route => $fn) {
            $class = $this;
            Route::get($route, function() use ($route, $class, $params) {
                return $class->call($route, $params);
            });
        }
    }

    function call($route, $params) {
        if (!empty($this->routes[$route])) {
            $this->routes[$route]($params);
        }
    }

    public static function autoload() {
        $directory = path('app') . '/libraries/jaequery/api';
        $classes = array_diff(scandir($directory), array('..', '.'));
        foreach ($classes AS $key => $class) {
            $x_class = explode(".", $class);
            self::load(ucfirst($x_class[0]));
        }
    }

    public static function load($name) {
        $class = "\Jaequery\Api\\{$name}";
        $api = new $class;
        $api->register();
    }

}
