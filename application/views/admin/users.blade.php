@section('content')

<div class=" main-content">

    <div class="row-fluid">
        <div class="row-fluid">


<h2>Basic Table</h2>
<table class="table table-first-column-number">
  <thead>
    <tr>
      <th style="padding-left: 1em;">#</th>
      <th>First</th>
      <th>Last</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>Mark</td>
      <td>Tompson</td>
      <td>the_mark7</td>
    </tr>
    <tr>
      <td>2</td>
      <td>Ashley</td>
      <td>Jacobs</td>
      <td>ash11927</td>
    </tr>
</tbody></table>

<h2>Sortable Table</h2>
<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper" role="grid"><table class="table table-first-column-number data-table sort display dataTable" id="DataTables_Table_0">
  <thead>
    <tr role="row"><th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="#: activate to sort column descending">#</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="First: activate to sort column ascending">First<span class="sort-icon"><span></span></span></th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Last: activate to sort column ascending">Last<span class="sort-icon"><span></span></span></th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending">Username<span class="sort-icon"><span></span></span></th></tr>
  </thead>
  <tbody role="alert" aria-live="polite" aria-relevant="all"><tr class="odd">
      <td class="  sorting_1">1</td>
      <td class=" ">Mark</td>
      <td class=" ">Tompson</td>
      <td class=" ">the_mark7</td>
    </tr><tr class="even">
      <td class="  sorting_1">2</td>
      <td class=" ">Ashley</td>
      <td class=" ">Jacobs</td>
      <td class=" ">ash11927</td>
    </tr></tbody></table></div>

<h2 style="margin-bottom: -1.75em;">Data Table</h2>
<div id="DataTables_Table_1_wrapper" class="dataTables_wrapper" role="grid"><div class=""><div class="dataTables_filter" id="DataTables_Table_1_filter"><label>Search: <input type="text" aria-controls="DataTables_Table_1"></label></div></div><table class="table table-first-column-number data-table display full dataTable" id="DataTables_Table_1">
  <thead>
    <tr role="row"><th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 16px;" aria-sort="ascending" aria-label="#: activate to sort column descending">#</th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 399px;" aria-label="First: activate to sort column ascending">First<span class="sort-icon"><span></span></span></th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 455px;" aria-label="Last: activate to sort column ascending">Last<span class="sort-icon"><span></span></span></th><th class="sorting" role="columnheader" tabindex="0" aria-controls="DataTables_Table_1" rowspan="1" colspan="1" style="width: 405px;" aria-label="User: activate to sort column ascending">User<span class="sort-icon"><span></span></span></th></tr>
  </thead>
  <tbody role="alert" aria-live="polite" aria-relevant="all"><tr class="odd">
      <td class="  sorting_1">1</td>
      <td class=" ">TONY</td>
      <td class=" ">MEYER</td>
      <td class=" ">teyer</td>
    </tr><tr class="even">
      <td class="  sorting_1">2</td>
      <td class=" ">KENDRA</td>
      <td class=" ">MARSHALL</td>
      <td class=" ">kmarshall</td>
    </tr><tr class="odd">
      <td class="  sorting_1">3</td>
      <td class=" ">NICHOLE</td>
      <td class=" ">BROOKS</td>
      <td class=" ">nibrooks</td>
    </tr><tr class="even">
      <td class="  sorting_1">4</td>
      <td class=" ">CHRISTIE</td>
      <td class=" ">GRAHAM</td>
      <td class=" ">chgraham</td>
    </tr><tr class="odd">
      <td class="  sorting_1">5</td>
      <td class=" ">RAY</td>
      <td class=" ">JENKINS</td>
      <td class=" ">rjenkins</td>
    </tr><tr class="even">
      <td class="  sorting_1">6</td>
      <td class=" ">KRYSTAL</td>
      <td class=" ">MORENO</td>
      <td class=" ">kmoreno</td>
    </tr><tr class="odd">
      <td class="  sorting_1">7</td>
      <td class=" ">MARCUS</td>
      <td class=" ">HERRERA</td>
      <td class=" ">mherrera</td>
    </tr><tr class="even">
      <td class="  sorting_1">8</td>
      <td class=" ">DAVID</td>
      <td class=" ">ROSE</td>
      <td class=" ">drose</td>
    </tr><tr class="odd">
      <td class="  sorting_1">9</td>
      <td class=" ">DEREK</td>
      <td class=" ">PAYNE</td>
      <td class=" ">dpayne</td>
    </tr><tr class="even">
      <td class="  sorting_1">10</td>
      <td class=" ">CHELSEA</td>
      <td class=" ">FOSTER</td>
      <td class=" ">cfoster</td>
    </tr></tbody></table><div class=""><div id="DataTables_Table_1_length" class="dataTables_length"><label>Show <select size="1" name="DataTables_Table_1_length" aria-controls="DataTables_Table_1"><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div class="dataTables_paginate paging_bootstrap pagination"><ul><li class="prev disabled"><a href="#">← Previous</a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a href="#">Next → </a></li></ul></div></div></div>

          </div></div></div>
@endsection