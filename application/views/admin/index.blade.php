@layout('admin.layout')
@section('content')
<div class="span8 main-content">

    <div class="row-fluid">
        <div class="row-fluid">
            <h2>Pending Deals
                <span class="info">You have 15 deals that will be closing soon.</span>
            </h2>
            <ul class="item-summary">
            

            
                <li>
                    <div class="overview">
                        <p class="main-detail">April 14th</p>
                        <p class="sub-detail">11:25 PM</p>
                        <span class="label label-success">Active</span> <span class="label label-info">New</span>
                    </div>
                    <div class="info">
                        <p>Trust fund photo letterpress, keytar raw skydiving denim grape keffiyeh etsy art base apple party ball before they.</p>
                        <a class="btn btn-mini" href="#">Show Details</a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            
                <li>
                    <div class="overview">
                        <p class="main-detail">Jan 24th</p>
                        <p class="sub-detail">4:34 PM</p>
                        <span class="label label-success">Active</span>
                    </div>
                    <div class="info">
                        <p>Trust fund photo letterpress, keytar raw skydiving denim grape keffiyeh etsy art base apple party ball before they.</p>
                        <a class="btn btn-mini" href="#">Show Details</a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            
                <li>
                    <div class="overview">
                        <p class="main-detail">May 9th</p>
                        <p class="sub-detail">6:53 PM</p>
                        <span class="label label-success">Active</span>
                    </div>
                    <div class="info">
                        <p>Trust fund photo letterpress, keytar raw skydiving denim grape keffiyeh etsy art base apple party ball before they.</p>
                        <a class="btn btn-mini" href="#">Show Details</a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            
                <li>
                    <div class="overview">
                        <p class="main-detail">June 3rd</p>
                        <p class="sub-detail">8:54 PM</p>
                        <span class="label label-success">Active</span>
                    </div>
                    <div class="info">
                        <p>Trust fund photo letterpress, keytar raw skydiving denim grape keffiyeh etsy art base apple party ball before they.</p>
                        <a class="btn btn-mini" href="#">Show Details</a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            
            </ul>
        </div>

        <div class="row-fluid">
            <div id="three-summaries" class="row-fluid">
                <div class="span5">
                    <h2>Mailboxes</h2>
                    <ul class="cards widget list">
                        <li><i class="icon-envelope"></i> New<span class="pull-right label label-important">23</span></li>
                        <li><i class="icon-share"></i> Follow Up<span class="pull-right label label-important">13</span></li>
                        <li><i class="icon-star-empty"></i> Starred<span class="pull-right label label-important">73</span></li>
                        <li><i class="icon-hdd"></i> All Messages<span class="pull-right label label-important">89</span></li>
                        <li><i class="icon-bookmark-empty"></i> Archived<span class="pull-right label label-important">24</span></li>
                        <li><i class="icon-thumbs-up"></i> Important<span class="pull-right label label-important">10</span></li>
                        <li><i class="icon-envelope"></i> Personal<span class="pull-right label label-important">39</span></li>
                    </ul>
                </div>
                <div class="span7">
                    <h2>Upcoming Events</h2>
                    <ul class="cards widget">
                        
                        
                        
                        <li>
                            <button class="btn pull-right btn-mini">Open</button>
                            <i class="pull-left muted icon-bar-chart"></i>
                            <div>
                                <p class="title"> Campaign Ends</p>
                                <p class="info">Saturday, July 22nd 12:45PM</p>
                            </div>
                        </li>
                        
                        <li>
                            <button class="btn pull-right btn-mini">Open</button>
                            <i class="pull-left muted icon-group"></i>
                            <div>
                                <p class="title"> User Training</p>
                                <p class="info">Saturday, July 22nd 12:45PM</p>
                            </div>
                        </li>
                        
                        <li>
                            <button class="btn pull-right btn-mini">Open</button>
                            <i class="pull-left muted icon-time"></i>
                            <div>
                                <p class="title"> Free Trial Ends</p>
                                <p class="info">Saturday, July 22nd 12:45PM</p>
                            </div>
                        </li>
                        
                        <li>
                            <button class="btn pull-right btn-mini">Open</button>
                            <i class="pull-left muted icon-gift"></i>
                            <div>
                                <p class="title"> User Appreciation Day</p>
                                <p class="info">Saturday, July 22nd 12:45PM</p>
                            </div>
                        </li>
                        
                        <li>
                            <button class="btn pull-right btn-mini">Open</button>
                            <i class="pull-left muted icon-plane"></i>
                            <div>
                                <p class="title"> Flight Training</p>
                                <p class="info">Saturday, July 22nd 12:45PM</p>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <h2>Inbox</h2>

            <table class="table table-first-column-check table-hover">
                <thead>
                    <tr>
                      <th class="span1"><i class="icon-star-empty"></i></th>
                      <th class="span2">From</th>
                      <th class="span9">Subject</th>
                      <th class="span2">Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                      <td><a href="#"><i class="icon-star-empty"></i></a></td>
                      <td><strong>John Doe</strong></td>
                      <td><strong>Message body goes in here</strong> <span class="label label-success pull-right" style="margin-left: .5em;">Follow Up </span> <span class="label label-info pull-right">Work</span></td>
                      <td><strong>11:23 PM</strong></td>
                    </tr>
                    <tr>
                      <td><a href="#"><i class="icon-star-empty"></i></a></td>
                      <td>John Doe</td>
                      <td>Message body goes in here <span class="label label-success pull-right">Follow Up</span></td>
                      <td>Sept4</td>
                    </tr>
                    <tr>
                      <td><a href="#"><i class="icon-star"></i></a></td>
                      <td><strong>John Doe</strong></td>
                      <td><strong>Message body goes in here</strong> <span class="label label-important pull-right">Spam</span></td>
                      <td><strong>Sept3</strong></td>
                    </tr>
                    <tr>
                      <td><a href="#"><i class="icon-star"></i></a></td>
                      <td><strong>John Doe</strong></td>
                      <td> <strong>Message body goes in here</strong> <span class="label pull-right">Personal</span></td>
                      <td><strong>Sept3</strong></td>
                    </tr>
                </tbody>
            </table>

            <h2>Top Users</h2>
            <div class="well widget" style="display: none;">
                <form class="form-inline" style="margin-bottom: 0px;">
                    <input class="input-xlarge" placeholder="Search All Users..." type="text">
                    <button class="btn" type="button"><i class="icon-search"></i> Go</button>
                </form>
            </div>
            <table class="table table-first-column-check">
              <thead>
                <tr>
                  <th><input type="checkbox"></th>
                  <th>First</th>
                  <th>Last</th>
                  <th>Username</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><input type="checkbox"></td>
                  <td>Mark</td>
                  <td>Tompson</td>
                  <td>the_mark7</td>
                </tr>
                <tr>
                  <td><input type="checkbox"></td>
                  <td>Ashley</td>
                  <td>Jacobs</td>
                  <td>ash11927</td>
                </tr>
                <tr>
                  <td><input type="checkbox"></td>
                  <td>Audrey</td>
                  <td>Ann</td>
                  <td>audann84</td>
                </tr>
                <tr>
                  <td><input type="checkbox"></td>
                  <td>John</td>
                  <td>Robinson</td>
                  <td>jr5527</td>
                </tr>
                <tr>
                  <td><input type="checkbox"></td>
                  <td>Aaron</td>
                  <td>Butler</td>
                  <td>aaron_butler</td>
                </tr>
                <tr>
                  <td><input type="checkbox"></td>
                  <td>Chris</td>
                  <td>Albert</td>
                  <td>cab79</td>
                </tr>
              </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
