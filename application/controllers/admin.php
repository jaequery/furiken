<?php

class Admin_Controller extends Base_Controller {

    public $layout = "admin.layout";

	public function action_index()
	{
	    $this->layout->nest('side', 'admin.partials.side');
	    $this->layout->nest('content', 'admin.index');
	}

	public function action_users()
	{
	    $this->layout->nest('content', 'admin.users');
	}

}